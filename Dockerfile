FROM alpine
MAINTAINER Taras Suhovenko
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
ADD app app
EXPOSE 8080 8080
ENTRYPOINT ["/app"]