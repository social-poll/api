package main

import (
	"context"
	"crypto/tls"
	"flag"
	"log"
	"net"
	"net/http"

	"github.com/joeshaw/envdecode"
	"gopkg.in/mgo.v2"
)

type contextKey struct {
	name string
}

var contextKeyAPIKey = &contextKey{"api-key"}

type Server struct {
	db *mgo.Session
}

type Config struct {
	MongoURL string `env:"MONGO_URL,default=localhost"`
}

func main() {
	var cfg Config
	if err := envdecode.Decode(&cfg); err != nil {
		log.Fatalln(err)
	}
	var addr = flag.String("addr", ":8080", "endpoint address")
	db, err := dialdb(cfg.MongoURL)
	if err != nil {
		log.Fatalln("failed to dial MongoDb:", err)
	}
	defer db.Close()
	s := &Server{db: db}
	mux := http.NewServeMux()
	mux.HandleFunc("/polls/", withCORS(withAPIKey(s.handlePolls)))
	log.Println("Starting web server on", *addr)
	serve := http.ListenAndServe(*addr, mux)
	log.Println("Stopping...", serve)
}

func APIKey(ctx context.Context) (string, bool) {
	key, ok := ctx.Value(contextKeyAPIKey).(string)
	return key, ok
}

func withAPIKey(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		key := r.URL.Query().Get("key")
		if !isValidAPIKey(key) {
			respondErr(w, r, http.StatusUnauthorized, "invalid API key")
			return
		}
		ctx := context.WithValue(r.Context(), contextKeyAPIKey, key)
		fn(w, r.WithContext(ctx))
	}
}

func withCORS(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Expose-Headers", "Location")
		fn(w, r)
	}
}

func isValidAPIKey(key string) bool {
	return key == "abc123"
}

func dialdb(mongoURL string) (*mgo.Session, error) {
	log.Println("dialing mongodb...", mongoURL)
	tlsConfig := &tls.Config{}
	tlsConfig.InsecureSkipVerify = true
	dialInfo, err := mgo.ParseURL(mongoURL)
	if err != nil {
		return nil, err
	}
	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
		return conn, err
	}
	return mgo.DialWithInfo(dialInfo)
}
